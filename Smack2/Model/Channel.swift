//
//  Channel.swift
//  Smack2
//
//  Created by MacBook Pro A on 9/14/17.
//  Copyright © 2017 MacBook Pro A. All rights reserved.
//

import Foundation

struct Channel {
    public private(set) var channelTitle: String!
    public private(set) var channelDescription: String!
    public private(set) var id: String!
}
